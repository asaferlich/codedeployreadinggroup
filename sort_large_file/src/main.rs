#[macro_use]
extern crate serde_derive;
extern crate docopt;
extern crate time;

use std::io::prelude::*;
use std::io::{Lines, BufReader, Write, BufWriter};
use std::fs::File;
use std::fs;
use std::path::Path;
use docopt::Docopt;
use std::str;
use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::fs::OpenOptions;
use time::PreciseTime;

const USAGE: &'static str = "
Sort Large File - This program will sort any size file by separating the sorting into chunks and will never require more than 1 GB of ram memory (it will rely on disk to chunk up the sorting)

Usage:
  sort_large_file --input <input> --output <output>
  sort_large_file (-h | --help)

Options:
  -h --help     Show this screen.
  -i --input    Input filename.
  -o --output   Output filename.
";

#[derive(Debug, Deserialize)]
struct Args {
    arg_input: String,
    arg_output: String,
}

const SORT_CHUNK_SIZE_IN_BYTES: usize = 256000000; //250 megabytes
const BUFREADER_CAPACITY_IN_BYTES: usize = 10240000; //10 megabytes
const FIVE_MEGABYTES_IN_BYTES: usize = 5120000; //5 megabytes
const TEN_MEGABYTES_IN_BYTES: usize = 10240000; //10 megabytes
const CHUNK_FILE_PREFIX: &'static str = "sort_large_file_chunk_";
const TEST_DIR_TO_CHUNK_FILES_INTO: &'static str = "/tmp/testdir3";

fn main() {
    let args: Args = Docopt::new(USAGE)
                            .and_then(|d| d.deserialize())
                            .unwrap_or_else(|e| e.exit());
    println!("{:?}", args);

    let input_file_path = &Path::new(&args.arg_input);
    let output_file_path = &Path::new(&args.arg_output);
    let _ = fs::remove_dir_all(TEST_DIR_TO_CHUNK_FILES_INTO);
    let _ = fs::create_dir(TEST_DIR_TO_CHUNK_FILES_INTO);
    let _ = fs::remove_file(&args.arg_output);
    let chunk_files_directory = Path::new(&TEST_DIR_TO_CHUNK_FILES_INTO);

    // whatever you want to do
    let start_chunk_sorting = PreciseTime::now();
    read_lines_from_file_in_chunks_then_sort_them_and_write_chunks_to_separate_files(input_file_path, chunk_files_directory, SORT_CHUNK_SIZE_IN_BYTES);
    let end_chunk_sorting = PreciseTime::now();
    println!("{} seconds to finish chunk sorting to individual files.", start_chunk_sorting.to(end_chunk_sorting).num_seconds());

    let start_merge_sorting_to_single_file = PreciseTime::now();
    combine_sorted_file_chunks_using_min_heap_across_tip_of_files(output_file_path, chunk_files_directory, SORT_CHUNK_SIZE_IN_BYTES);
    let end_merge_sorting_to_single_file = PreciseTime::now();
    println!("{} seconds to merge sorting to single file.", start_merge_sorting_to_single_file.to(end_merge_sorting_to_single_file).num_seconds());
}

fn read_lines_from_file_in_chunks_then_sort_them_and_write_chunks_to_separate_files<P>(filename: P, chunk_files_directory: &Path, chunk_size: usize) -> ()
where
P: AsRef<Path>,
{
    let file = File::open(filename).expect("no such file");
    let buf = BufReader::with_capacity(BUFREADER_CAPACITY_IN_BYTES, file);

    let mut chunk_of_strings: Vec<String> = Vec::with_capacity(chunk_size);
    let mut byte_count = 0;
    let mut file_count = 0;
    //Note lines allocates a new one everytime, Drake suggested using read_until which allows you
    //to reuse the same vector

    for line in buf.lines() {
        let mut line = line.expect("Could not parse line");
        line.push('\n');
        if byte_count + line.len() > chunk_size {
            chunk_of_strings.sort();
            file_count += 1;
            println!("Writing chunk number {}", file_count);
            write_to_file(&chunk_files_directory.join(format!("{}{}.txt", CHUNK_FILE_PREFIX, file_count)), &chunk_of_strings);
            byte_count = 0;
            chunk_of_strings.clear();
        }

        byte_count += line.len();
        chunk_of_strings.push(line)
    }

    if !chunk_of_strings.is_empty() {
        chunk_of_strings.sort();
        file_count += 1;
        println!("Writing chunk number {}", file_count);
        write_to_file(&chunk_files_directory.join(format!("{}{}.txt", CHUNK_FILE_PREFIX, file_count)), &chunk_of_strings);
    }
}

fn write_to_file(filename: &Path, lines: &Vec<String>) -> () {
    println!("Writing lines to file {}", filename.to_str().unwrap());
    let mut writer = BufWriter::with_capacity(BUFREADER_CAPACITY_IN_BYTES,
                                              File::create(filename).expect("Unable to create file"));
    for line in lines {
        writer.write_all(line.as_bytes()).expect("Unable to write data");
    }
}

struct LineWithIterator {
    line: String,
    line_iterator: Lines<BufReader<File>>
}

impl PartialEq for LineWithIterator {
    fn eq(&self, other: &LineWithIterator) -> bool {
        self.line == other.line
    }
}
impl Eq for LineWithIterator {}

impl Ord for LineWithIterator {
    fn cmp(&self, other: &LineWithIterator) -> Ordering {
        other.line.cmp(&self.line)
    }
}

impl PartialOrd for LineWithIterator {
    fn partial_cmp(&self, other: &LineWithIterator) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

fn combine_sorted_file_chunks_using_min_heap_across_tip_of_files(output_file: &Path, chunk_files_directory: &Path, chunk_size: usize) -> () {
    let paths = fs::read_dir(chunk_files_directory).unwrap();
    let files: Vec<File> = paths.map(|p| File::open(p.unwrap().path()).unwrap()).collect();
    let mut bufreaders = vec![];
    for file in files {
        bufreaders.push(BufReader::with_capacity(TEN_MEGABYTES_IN_BYTES, file));
    }
    let mut line_iterators = vec![];
    for bufreader in bufreaders {
        line_iterators.push(bufreader.lines());
    }
    let mut lines_with_iterators_priority_queue = BinaryHeap::new();
    for mut line_iterator in line_iterators {
        match line_iterator.next() {
            Some(line) => {
                let mut line = line.expect("Could not parse line");
                line.push('\n');
                lines_with_iterators_priority_queue.push(LineWithIterator { line: line, line_iterator: line_iterator })
            }
            _ => {}
        }
    }

    let mut absolutely_sorted_lines_buffer: Vec<String> = Vec::with_capacity(chunk_size);
    let mut byte_count = 0;

    if !Path::new(output_file).exists() {
        File::create(output_file).expect("Unable to create file");
    }
    let mut options = OpenOptions::new();
    options.write(true).append(true);
    let file = match options.open(output_file) {
        Ok(file) => file,
        Err(..) => panic!("at the Disco"),
    };
    let mut writer = BufWriter::with_capacity(BUFREADER_CAPACITY_IN_BYTES, &file);
    let mut append_count = 0;

    while let Some(LineWithIterator { line, mut line_iterator }) = lines_with_iterators_priority_queue.pop() {
        if byte_count + line.len() > chunk_size {
            append_count += 1;
            println!("Appending lines to file for time number {}", append_count);
            for absolutely_sorted_line in &absolutely_sorted_lines_buffer {
                writer.write_all(absolutely_sorted_line.as_bytes()).expect("Unable to write data");
            }
            byte_count = 0;
            absolutely_sorted_lines_buffer.clear();
        }
        byte_count += line.len();
        absolutely_sorted_lines_buffer.push(line);

        match line_iterator.next() {
            Some(next_line) => {
                let mut next_line = next_line.expect("Could not parse line");
                next_line.push('\n');
                lines_with_iterators_priority_queue.push(LineWithIterator { line: next_line, line_iterator: line_iterator })
            }
            _ => {}
        }
    }

    if !absolutely_sorted_lines_buffer.is_empty() {
        append_count += 1;
        println!("Appending lines to file for time number {}", append_count);
        for absolutely_sorted_line in &absolutely_sorted_lines_buffer {
            writer.write_all(absolutely_sorted_line.as_bytes()).expect("Unable to write data");
        }
    }
}
